from django.views.generic.base import TemplateView

# Create your views here.
class Teamview(TemplateView):
    '''Vista nuestro equipo'''
    template_name = "team.html"

class Aboutview(TemplateView):
    #'''Vista acerca de nosotros'''
    template_name = "about.html"


class Treatmentsview(TemplateView):
    #'''Vista tratamientos'''
    template_name = "treatments.html"


class Offersview(TemplateView):
    '''Vista ofertas'''
    template_name = "offers.html"


class Contactview(TemplateView):
    template_name = "contact.html"
