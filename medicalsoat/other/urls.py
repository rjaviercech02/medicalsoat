from django .conf               import settings
from django .urls               import path 
from django .conf  .urls.static import static
from .views              import *

urlpatterns = [
    path('nuestro-equipo', Teamview.as_view(), name='team'),
    path('nosotros', Aboutview.as_view(), name='about'),
    path('tratamientos', Treatmentsview.as_view(), name='treatments'),
    path('ofertas', Offersview.as_view(), name='offerts'),
    path('contactos', Contactview.as_view(), name='contact'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
